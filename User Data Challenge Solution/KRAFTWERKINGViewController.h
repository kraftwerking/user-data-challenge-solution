//
//  ViewController.h
//  User Data Challenge Solution
//
//  Created by RJ Militante on 1/26/15.
//  Copyright (c) 2015 Kraftwerking LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KRAFTWERKINGViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSArray *users;
@property (strong, nonatomic) IBOutlet UITableView *tableView;


@end

